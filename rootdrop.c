#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
   
int main(int argc, char *argv[])
{
   FILE *fp;
   int i;


   fp = fopen(argv[1], "w");
   
   if (fp == NULL) {
      printf("I couldn't open %s .\n", argv[1]);
      exit(0);
   }

   fprintf(fp, "UID %d  EUID %d \n", getuid(), geteuid() );

   for (i=0; i<=10; ++i){
     fprintf(fp, "%d \n", i);
   }
  return 0;
}

